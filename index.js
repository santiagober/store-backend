const express = require('express')
const passport = require('passport')
const v1 = require('./v1')
const morgan = require('morgan')
const mongoose = require('mongoose')
require('./datalayer/connections/mysql')
// const winston = require('./config/winston')

const app = express()
const router = express.Router()

// app.use(morgan(':remote-addr :method :url', { stream: winston.stream }))
app.use(morgan('dev'))

// TODO: TRASLADAR CONNECT A CREATE CONNECTION
// mongoose.connect(process.env.DB_CONNECT, {
//   useUnifiedTopology: true,
//   useNewUrlParser: true,
//   autoIndex: false
// })
//   .then(() => {
//     console.log('info', 'connected to mongodb');
//   })
//   .catch((error) => {
//     console.log('info', 'error connecting to db: ' + error);
//   });

// mongoose.set('autoCreate', false)
router.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Methods', 'POST, PUT, GET, DELETE, OPTIONS')
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization')

  next()
})


app.use(express.json())
app.use(express.urlencoded({ extended: true }))
// app.use((req, res, next) => {
//   console.log(req.headers)
//   next()
// })
app.listen(process.env.PORT, () => {
  console.log('app listening in port: ' + process.env.PORT)
})

// -------------------- load password config --------------------------------
require('./config/passport')(passport)

app.use(passport.initialize())

router.use('/api/v1', v1)

app.use('/', router)
