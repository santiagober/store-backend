const passport = require('passport')
const { Types } = require('mongoose')
const path = require('path')
const fs = require('fs')
const JwtStrategy = require('passport-jwt').Strategy
const ExtractJwt = require('passport-jwt').ExtractJwt
const { validPass } = require('./../utils')
const mysql = require('./../datalayer/connections/mysql')

const { Users } = require('../datalayer/models')

const pathToKey = path.join(__dirname, '../utils/', 'id_rsa_pub.pem')
const PUB_KEY = fs.readFileSync(pathToKey, 'utf8')

const options = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: PUB_KEY,
  algorithms: ['RS256']
}

const strategy = new JwtStrategy(options, async (payload, done) => {
  const connection = await mysql.connection()
  let obj
  try {
    obj = await connection.query("SELECT username, companyId, agencyId FROM user WHERE id = ? AND removed = ? ;", [payload.sub, false])
  } catch (err) {
    done(err, null)
  } finally {
    await connection.release()
  }
  if (obj.length) {
    return done(null, {
      ...obj[0],
      subc: payload.subc,
      suba: payload.suba,
    })
  } else {
    return done(null, false)
  }
  // Users.findById(payload.sub, { username: 1 })
  //   .then((user) => {
  //     if (user)
  //       return done(null, user)
  //     else
  //       return done(null, false)
  //   })
  //   .catch(err => done(err, null))
})

module.exports = (passport) => {
  passport.use(strategy)
}

// passport.use(new LocalStrategy({
//   usernameField: 'username',
//   passwordField: 'password'
// }, async (username, password, done) => {
//   try {
//     if (username) {
//       const user = await Users.findOne({ username: username }, { username: 1, password: 1, salt: 1 })
//       if (user) {
//         const isValid = validPass(password, user.password, user.salt)
//         if (isValid)
//           return done(null, user)
//         else
//           return done(null, false)
//       } else {
//         return done(null, false)
//       }
//     }
//   } catch (err) {
//     return done(err)
//   }
// }))
