const winston = require('winston');
const tsFormat = () => (new Date().toLocaleTimeString)

const options = {
  // file: {
  //   level: 'info',
  //   filename: `${appRoot}/logs/app.log`,
  //   handleExceptions: true,
  //   json: true,
  //   maxsize: 5242880, // 5MB
  //   maxFiles: 5,
  //   colorize: false,
  // },
  console: {
    level: 'info',
    handleExceptions: true,
    json: false,
    timestamp: tsFormat,
    colorize: true,
  },
};

const logger = new winston.createLogger({
  transports: [
    // new winston.transports.File(options.file),
    new winston.transports.Console(options.console)
  ],
  exitOnError: false, // do not exit on handled exceptions
});

logger.stream = {
  write: function (message, encoding) {
    // use the 'info' log level so the output will be picked up by both transports (file and console)
    logger.info(message);
  },
};

module.exports = logger;
