const {
  MYSQL_URL,
  MYSQL_USER,
  MYSQL_PASS,
  MYSQL_DB,
  MYSQL_LIMIT
} = process.env

const dbConfig = {
  connectionLimit: MYSQL_LIMIT,
  host: MYSQL_URL,
  user: MYSQL_USER,
  password: MYSQL_PASS,
  database: MYSQL_DB,
  multipleStatements: true,
}

module.exports = { dbConfig }