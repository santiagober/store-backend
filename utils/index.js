const crypto = require('crypto')
const jwt = require('jsonwebtoken')
const fs = require('fs')
const path = require('path')

const genPass = (pass) => {
  const salt = crypto.randomBytes(32).toString('hex')
  const hash = crypto.pbkdf2Sync(pass, salt, 10000, 64, 'sha512').toString('hex')

  return { salt, hash }
}

const validPass = (pass, hash, salt) => {
  const hashVerify = crypto.pbkdf2Sync(pass, salt, 10000, 64, 'sha512').toString('hex')

  return hash === hashVerify
}

const validEmailFormat = email => /^(?=.{1,254}$)(?=.{1,64}@)[-!#$%&'*+/0-9=?A-Z^_`a-z{|}~]+(\.[-!#$%&'*+/0-9=?A-Z^_`a-z{|}~]+)*@[A-Za-z0-9]([A-Za-z0-9-]{0,61}[A-Za-z0-9])?(\.[A-Za-z0-9]([A-Za-z0-9-]{0,61}[A-Za-z0-9])?)*$/.test(email)

const generateToken = (user) => {
  const expiresIn = 1
  const payload = {
    sub: user.id.toString(),
    suba: user.agencyId,
    subc: user.companyId,
    username: user.username,
    iat: Date.now()
  }
  const pathToKey = path.join(__dirname, 'id_rsa_priv.pem')
  const priv_key = fs.readFileSync(pathToKey, 'utf8')
  const signedToken = jwt.sign(payload, priv_key, { expiresIn, algorithm: 'RS256' })

  return {
    // token: 'Bearer ' + signedToken,
    token: signedToken,
    expires: expiresIn
  }
}

module.exports = {
  genPass,
  validPass,
  validEmailFormat,
  generateToken
}