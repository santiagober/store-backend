const mysql = require('mysql');
const util = require('util');
const { dbConfig } = require('./../../config/mysql')

const pool = mysql.createPool(dbConfig);

pool.on('acquire', function (connection) {
  console.log('Connection %d acquired', connection.threadId);
})
pool.on('connection', function (connection) {
  connection.query('SET SESSION auto_increment_increment=1')
})
pool.on('enqueue', function () {
  console.log('Waiting for available connection slot');
})
pool.on('release', function (connection) {
  console.log('Connection %d released', connection.threadId);
})

const connection = () => {
  return new Promise((resolve, reject) => {
    pool.getConnection((err, connection) => {
      if (err) reject(err);
      console.log("MySQL pool connected: threadId " + connection.threadId);
      const query = (sql, binding) => {
        return new Promise((resolve, reject) => {
          connection.query(sql, binding, (err, result) => {
            if (err) reject(err);
            resolve(result);
          });
        });
      };
      const release = () => {
        return new Promise((resolve, reject) => {
          if (err) reject(err);
          console.log("MySQL pool released: threadId " + connection.threadId);
          resolve(connection.release());
        });
      };
      resolve({ query, release });
    });
  });
};

const query = (sql, binding) => {
  return new Promise((resolve, reject) => {
    pool.query(sql, binding, (err, result, fields) => {
      if (err) reject(err);
      resolve(result);
    });
  });
};

module.exports = { pool, connection, query };
