const { Users } = require('./user')
const { Companies } = require('./company')
const { Agencies } = require('./agency')
const { Persons } = require('./person')
const { Products } = require('./product')

module.exports = {
    Users,
    Persons,
    Companies,
    Agencies,
    Products,
}