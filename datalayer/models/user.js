const mongoose = require('mongoose')
const { connectMongoStore } = require('./../connections/mongo')
console.log('userrrrrr')
// mongoose.createConnection(connectMongoStore.conn, connectMongoStore.options)
//   .then(() => {
//     console.log('info', 'connected to mongodb');
//   })
//   .catch((error) => {
//     console.log('info', 'error connecting to db: ' + error);
//   });

const users = new mongoose.Schema({
  email: {
    index: true,
    type: String,
  },
  username: { type: String },
  password: { type: String },
  salt: { type: String },
  status: { type: Number },
  numberPhone: { type: String },
  delete: {
    type: Boolean,
    default: false
  },
  personId: {
    type: mongoose.Schema.Types.ObjectId
  },
  companyId: {
    type: String
    // type: mongoose.Schema.Types.ObjectId
  },
  agencyId: {
    type: String
    // type: mongoose.Schema.Types.ObjectId
  }
}, {
  timestamps: true
})

const Users = mongoose.model('User', users)

module.exports = { Users }
