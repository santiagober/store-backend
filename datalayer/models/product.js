const mongoose = require('mongoose')

console.log('product')

const price = new mongoose.Schema({
  utility: {
    type: Number,
    max: 100
  },
  salePrice: { type: Number },
  fullSalePrice: { type: Number },
  wholesaleUnit: { type: Number }
})

const products = new mongoose.Schema({
  description: { type: String },
  family: { type: String },
  alternativeKey: { type: String },
  primaryKey: { type: String },
  category: { type: String },
  subCategory: { type: Number },
  purchaseUnit: { type: String },
  salesUnit: { type: String },
  imgs: [{ type: String }],
  factor: { type: Number },
  minInventory: { type: Number },
  maxInventory: { type: Number },
  stock: {
    type: Number,
    'default': 0
  },
  location: { type: String },
  service: {
    type: Boolean,
    'default': false
  },
  neto: {
    type: Boolean,
    'default': false
  },
  prices: [price],
  // purchPricewoTaxUnitPurch: { type: Number },
  // purchPricewoTaxUnitSale: { type: Number },
  delete: {
    type: Boolean,
    default: false
  }
}, {
  timestamps: true
})

const Products = mongoose.model('Product', products)

module.exports = { Products }
