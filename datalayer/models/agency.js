const mongoose = require('mongoose')
console.log('agencyyyy')

const agencies = new mongoose.Schema({
  name: { type: String },
  companyId: { type: mongoose.Schema.Types.ObjectId },
  shortId: { type: String, index: true },
}, {
  timestamps: true
})

const Agencies = mongoose.model('Agency', agencies)

module.exports = { Agencies }
