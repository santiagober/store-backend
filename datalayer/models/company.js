const mongoose = require('mongoose')
// const { connectMongoStore } = require('./../connections/mongo')
console.log('companyyyyyy')

const location = new mongoose.Schema({
  region: { type: mongoose.Schema.Types.ObjectId },
  province: { type: mongoose.Schema.Types.ObjectId },
  district: { type: mongoose.Schema.Types.ObjectId }
})

const companies = new mongoose.Schema({
  name: { type: String },
  fiscalData: {
    RUC: { type: String },
    businessName: { type: String },
  },
  shortId: { type: String, index: true },
  address: { type: String },
  email: { type: String },
  contact: {
    personId: { Type: mongoose.Schema.Types.ObjectId },
    email: { type: String },
    numberPhone: { type: String }
  },
  location: location,
  businessLine: { type: mongoose.Schema.Types.ObjectId },
  logo: { type: String }
}, {
  timestamps: true
})

const Companies = mongoose.model('Company', companies)

module.exports = { Companies }
