const mongoose = require('mongoose')
// const { connectMongoStore } = require('./../connections/mongo')
console.log('providerrrrrrrrrr')

const providers = new mongoose.Schema({
  name: { type: String },
  alias: { type: String },
  RUC: { type: String },
  email: { type: String },
  numberPhone: { type: String },
  contact: {
    personId: { Type: mongoose.Schema.Types.ObjectId },
    email: { type: String },
    numberPhone: { type: String }
  },
  address: { type: String },
}, {
  timestamps: true
})

const Providers = mongoose.model('Provider', providers)

module.exports = { Providers }
