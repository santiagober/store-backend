const mongoose = require('mongoose')
// const { connectMongoStore } = require('../connections/mongo')
console.log('personnnnn')
// mongoose.createConnection(connectMongoStore.conn, connectMongoStore.options)

const persons = new mongoose.Schema({
  numDoc: { type: String },
  firstName: { type: String },
  lastName: { type: String },
  address: { type: String },
  docType: { type: Number },
  delete: {
    type: Boolean,
    default: false
  }
}, {
  timestamps: true
})

const Persons = mongoose.model('Person', persons)

module.exports = { Persons }
