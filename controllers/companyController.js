// const { Types } = require('mongoose')

// const {
//   Companies,
//   Agencies
// } = require('./../datalayer/models')
const mysql = require('./../datalayer/connections/mysql')

const createCompany = async (company, agency) => {
  // const { name, fiscalData, shortId, address, email, contact, location, businessLine, logo } = company
  // const { name, shortId } = agency
  const connection = await mysql.connection()
  let newAgen, newComp
  try {
    await connection.query("START TRANSACTION");
    newComp = await connection.query("INSERT INTO company SET ? ;", [company])
    newAgen = await connection.query("INSERT INTO agency(name, companyId, shortId) values(?,?,?) ;", [agency.name, newComp.insertId, agency.shortId])
    await connection.query("COMMIT")
  } catch (err) {
    await connection.query("ROLLBACK")
    throw (err)
  } finally {
    await connection.release()
  }
  // const companyCreated = await Companies.create(company)
  // const agencyCreated = await Agencies.create({
  //   ...agency,
  //   companyId: Types.ObjectId(companyCreated._id)
  // })

  return {
    success: true,
    company: newComp.insertId,
    agency: newAgen.insertId
  }
}

module.exports = {
  createCompany
}