const mysql = require('./../datalayer/connections/mysql')

const {
  Products
} = require('./../datalayer/models')

const searchProducts = async (search, limit, skip) => {
  const connection = await mysql.connection()
  const str = search === '' ? '.*' : search
  try {
    const products = await connection.query("SELECT id, primaryKey, alternativeKey, description ,category from product WHERE removed = ? " +
      " AND (description REGEXP ? OR alternativeKey REGEXP ? OR primaryKey REGEXP ?) LIMIT ?, ?;", [false, str, str, str, skip, skip + limit])
    return { products }
  } catch (err) {
    throw (err)
  } finally {
    await connection.release()
  }
  // const query = {
  //   $and: [
  //     {
  //       $or: [
  //         {
  //           description: {
  //             $regex: search,
  //             $options: 'i'
  //           }
  //         }, {
  //           alternativeKey: {
  //             $regex: search,
  //             $options: 'i'
  //           }
  //         }, {
  //           primaryKey: {
  //             $regex: search,
  //             $options: 'i'
  //           }
  //         }
  //       ]
  //     }, {
  //       delete: false
  //     }
  //   ]
  // }

  // const products = await Products.find(query, { primaryKey: 1, alternativeKey: 1, description: 1, category: 1, prices: 1 }, { skip, limit }).lean()
  // console.log(products)

  // return {
  //   products: products.map(prod => ({
  //     ...prod,
  //     prices: prod.prices[0]
  //   }))
  // }
}

const registerProduct = async (product) => {
  const {
    alternativeKey,
    primaryKey,
    category,
    description,
    factor,
    family,
    location,
    minINventory,
    maxInventory,
    neto,
    purchasePrice,
    purchaseUnit,
    saleUnit,
    service,
    stock,
    subcategory,
    uucomp,
    uuagen,
  } = product
  const connection = await mysql.connection()
  try {
    await connection.query("INSERT INTO product(" +
      "alternativeKey,primaryKey,category,description,factor,family,location,minINventory,maxInventory,neto,purchasePrice,purchaseUnit," +
      "saleUnit,service,stock,subcategory,companyId,agencyId) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);", [
      alternativeKey,
      primaryKey,
      category,
      description,
      factor,
      family,
      location,
      minINventory,
      maxInventory,
      neto,
      purchasePrice,
      purchaseUnit,
      saleUnit,
      service,
      stock,
      subcategory,
      uucomp,
      uuagen,
    ])
  } catch (err) {
    throw (err)
  } finally {
    await connection.release()
  }

  return {
    success: true,
    message: 'Operacion realizada con exito !!!'
  }
}

const deleteProduct = async (id) => {
  const connection = await mysql.connection()
  // await Products.findByIdAndUpdate(_id, { delete: true })
  try {
    await connection.query("UPDATE product set removed = ? WHERE id = ?;", [true, id])
  } catch (err) {
    throw (err)
  } finally {
    await connection.release()
  }
  return {
    success: true,
    message: 'Eliminado correctamente.'
  }
}

const findProduct = async (id, companyId, agencyId) => {
  const connection = await mysql.connection()
  try {
    const [product] = await connection.query("SELECT id,description,family,alternativeKey,primaryKey,category,subcategory," +
      "purchaseUnit,saleUnit,factor,purchasePrice, minInventory,maxInventory,stock,location,service,neto,removed FROM product  WHERE id = ? AND companyId = ? AND agencyId = ? AND removed = ?;", [id, companyId, agencyId, false])
    return {
      data: product,
      success: true,
    }
  } catch (err) {
    throw (err)
  } finally {
    await connection.release()
  }
}

const updateProduct = async (product, id) => {
  const { family, category, subcategory, purchaseUnit, saleUnit, factor, minInventory, maxInventory, stock, location, service, neto, uucomp, uuagen } = product
  const connection = await mysql.connection()
  try {
    await connection.query("UPDATE product SET family = ?, category = ?, subcategory = ?, purchaseUnit = ?, saleUnit = ?, factor = ?, " +
      " minInventory = ?, maxInventory = ?, stock = ?, location = ?, service = ?, neto = ?  WHERE id = ? AND companyId = ? " +
      "AND agencyId = ? AND removed = ?;",
      [family, category, subcategory, purchaseUnit, saleUnit, factor, minInventory, maxInventory, stock, location, service, neto, parseInt(id), uucomp, uuagen, false])
    return {
      message: 'Producto actualizado correctamente.',
      success: true,
    }
  } catch (err) {
    console.log(err)
    throw (err)
  } finally {
    await connection.release()
  }
}

module.exports = {
  searchProducts,
  updateProduct,
  deleteProduct,
  registerProduct,
  findProduct,
}
