const {
  Companies,
  Agencies,
  Persons,
  Users
} = require('./../datalayer/models')

const mysql = require('./../datalayer/connections/mysql')

const {
  validEmailFormat,
  validPass
} = require('./../utils')

const {
  genPass,
  generateToken
} = require('./../utils')

const { Types, Connection } = require('mongoose')

const logIn = async (username, password, companyId, agencyId) => {
  let obj
  const connection = await mysql.connection()
  try {
    if (username && password && companyId && agencyId) {
      const [[result]] = await connection.query('CALL login(?,?,?);', [companyId, agencyId, username])
      if (!result.erro) {
        const verify = validPass(password, result.password, result.salt)
        if (verify) {
          const { token, expires } = generateToken(result)
          obj = {
            token,
            expires,
            uucomp: result.companyId,
            uuagen: result.agencyId,
            igv: result.igv,
            success: true
          }
        } else {
          obj = {
            message: 'usuario o contraseña incorrectos',
            success: false
          }
        }
      } else {
        obj = {
          message: result.msge,
          success: false
        }
      }
      // query = "SELECT id companyId FROM company c WHERE c.shortId = ? AND c.removed = ?; SELECT id agencyId FROM agency a WHERE a.shortId = ? AND a.removed = ?;"
      // const [compArr, agenArr] = await connection.query(query, [companyId, false, agencyId, false])
      // if (compArr.length && agenArr.length) {
      //   const [company] = compArr
      //   const [agency] = agenArr
      //   query = "SELECT id, username, password, salt, companyId, agencyId FROM user WHERE username = ? AND companyId = ? AND agencyId = ? AND removed = ? LIMIT 1;"
      //   const [RowDataPacket] = await connection.query(query, [username, company.companyId, agency.agencyId, false])
      //   if (RowDataPacket) {
      //     const verify = validPass(password, RowDataPacket.password, RowDataPacket.salt)
      //     if (verify) {
      //       const { token, expires } = generateToken(RowDataPacket)
      //       obj = {
      //         token,
      //         expires,
      //         uucomp: company.companyId,
      //         uuagen: agency.agencyId,
      //         success: true
      //       }
      //     } else {
      //       obj = {
      //         message: 'usuario o contraseña incorrectos',
      //         success: false
      //       }
      //     }
      //   } else {
      //     obj = {
      //       message: 'usuario o contraseña incorrectos',
      //       success: false
      //     }
      //   }
      // } else {
      //   obj = {
      //     message: 'Error en parametros1',
      //     success: false
      //   }
      // }
    } else {
      obj = {
        message: 'Error en parametros',
        success: false
      }
    }
  } catch (err) {
    throw (err)
  } finally {
    await connection.release();
  }
  return obj
}

const verifyEmail = async (email) => {
  if (email && validEmailFormat(email)) {
    const user = await Users.findOne({ email, delete: false }).lean()
    return (user)
  } else {
    return false
  }
}

const registerUser = async (username, password, agencyId, companyId, email, firstName, lastName, address, numberPhone, docType, numDoc) => {
  const connection = await mysql.connection();
  let query, obj, newPerson
  try {
    if (username && password && agencyId && companyId && email && firstName && lastName && docType && numDoc) {
      if (validEmailFormat(email)) {
        query = "SELECT username,personId FROM user u WHERE u.username = ? AND u.companyId = ? AND u.agencyId = ? AND u.removed = ? ;"
        const user = await connection.query(query, [username, companyId, agencyId, false, false, docType, numDoc])
        if (user.length) {
          obj = { message: 'Usuario ya existe', success: false }
        } else {
          query = "SELECT id, firstName, lastName FROM person p WHERE p.removed = ? AND p.docType = ? AND p.numDoc = ? LIMIT 1;"
          const person = await connection.query(query, [false, docType, numDoc])
          await connection.query("START TRANSACTION");
          if ((!person.length)) {
            query = "INSERT INTO person(firstName, lastName, address, docType, numDoc) values(?,?,?,?,?); "
            newPerson = await connection.query(query, [firstName, lastName, address, docType, numDoc])
          }
          const { hash, salt } = genPass(password)
          query = "INSERT INTO user(email, username, password, salt, status, numberPhone, personId, companyId, agencyId) values(?,?,?,?,?,?,?,?,?); "
          await connection.query(query, [email, username, hash, salt, 1, numberPhone, newPerson ? newPerson.insertId : person[0]['id'], companyId, agencyId])
          await connection.query("COMMIT");
          const { token, expires } = generateToken({ id: newPerson ? newPerson.insertId : person[0]['id'], username })
          obj = {
            token,
            expires,
            success: true,
            message: 'Usuario creado correctamente'
          }
        }
        // const user = await Users.findOne({ username, agencyId, companyId, email })
        // var person = await Persons.findOne({ delete: false, docType, numDoc })
        // if (!(person)) {
        //   person = await Persons.create({ firstName, lastName, address, docType, numDoc })
        // }
        // const users = await Users.find({ delete: false, email })
        // if (users.length) {
        //   return { message: 'Email ya existe', success: false }
        // } else {
        //   const user = await Users.findOne({ username, agencyId, companyId, delete: false })
        //   if (user) {
        //     return { message: 'Usuario ya existe', success: false }
        //   } else {
        //     const { hash, salt } = genPass(password)
        //     const user2 = await Users.create({
        //       personId: Types.ObjectId(person._id),
        //       username,
        //       agencyId,
        //       companyId,
        //       numberPhone,
        //       email,
        //       password: hash,
        //       salt
        //     })
        //     // const { token, expires } = generateToken(user2)
        //     return {
        //       // token,
        //       // expires,
        //       success: true,
        //       message: 'Usuario creado correctamente'
        //     }
        //   }
        // }
      } else {
        obj = { message: 'Formato de email no valido', success: false }
      }
    } else {
      obj = { message: 'Error en parametros', success: false }
    }
  } catch (err) {
    await connection.query("ROLLBACK")
    obj = { message: err.message, success: false }
  } finally {
    await connection.release()
  }
  return obj
}

const getCompanyParams = async (companyId, agencyId) => {
  const connection = await mysql.connection();
  const [params] = await connection.query('SELECT igv FROM companyParams c WHERE c.companyId = ? AND removed = ?;', [companyId, false])
  return {
    igv: params.igv
  }
}


module.exports = {
  logIn,
  getCompanyParams,
  registerUser
}