const express = require('express')

const {
  searchProducts,
  updateProduct,
  registerProduct,
  deleteProduct,
  findProduct,
} = require('./../../controllers/productController')

const agency = require('../../datalayer/models/agency')

const router = express.Router()

router.post('/register', async (req, res) => {
  const { product } = req.body
  try {
    const { success, message } = await registerProduct(product)
    res.send({ success, message })
  } catch (err) {
    res.status(500).send({ message: err.message, success: false })
  }
})

router.post('/:id', async (req, res) => {
  const { product } = req.body
  const { id } = req.params
  try {
    const { success, message } = await updateProduct(product, id)
    res.send({ success, message })
  } catch (err) {
    res.status(500).send({ message: err.message, success: false })
  }
})

router.get('/list', async (req, res) => {
  const {
    search,
    limit,
    skip,
  } = req.query
  try {
    const { products } = await searchProducts(search, parseInt(limit), parseInt(skip))
    res.status(200).send({ success: true, products })
  } catch (err) {
    res.status(500).send({ message: err.message, success: false })
  }
})

router.delete('/:id', async (req, res) => {
  const {
    id
  } = req.params
  try {
    const { success, message } = await deleteProduct(id)
    res.status(200).send({ success, message })
  } catch (err) {
    res.status(500).send({ message: err.message, success: false })
  }
})

router.get('/:id', async (req, res) => {
  const {
    id
  } = req.params

  const {
    uucomp,
    uuagen,
  } = req.query
  try {
    const { data, success } = await findProduct(parseInt(id), parseInt(uucomp), parseInt(uuagen))
    res.status(200).send({ data, success })
  } catch (err) {
    res.status(500).send({ message: err.message, success: false })
  }
})

module.exports = router