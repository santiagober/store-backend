const express = require('express')
const user = require('./user')
const company = require('./company')
const product = require('./product')

const router = express.Router()

router.use('/user', user)
router.use('/company', company)
router.use('/product', product)

module.exports = router
