const express = require('express')
const passport = require('passport')
// const jwi = require('jsonwebtoken')

const {
  logIn,
  getUser,
  // verifyEmail,
  getCompanyParams,
  registerUser
} = require('./../../controllers/userController')

const router = express.Router()

//TODO: agregar validacion de que el token enviado pertenezca al mismo usuario de la peticion y no a otro(ahora si permite)
router.post('/login', async (req, res) => {
  const { username, password, companyId, agencyId } = req.body
  try {
    const {
      token,
      expires,
      uucomp,
      uuagen,
      igv,
      success,
      message
    } = await logIn(username, password, companyId, agencyId)
    res.send({
      token,
      expires,
      uucomp,
      igv,
      uuagen,
      success,
      message
    })
  } catch (err) {
    res.status(500).send({ message: err.message, success: false })
  }
})

// router.post('/logout', async (req, res) => {
//   const { refreshToken } = req.body
//   const { authorization } = req.headers

//   const accessToken = authorization.slice(7, authorization.length)

//   try {
//     res.send({ success: true })
//   } catch (error) {
//     res.status(500).send({ error: '', success: false })
//   }
// })

router.post('/protected', passport.authenticate('jwt', { session: false }), async (req, res) => {
  try {
    const { user } = req
    const { uucomp, uuagen } = req.body
    if (parseInt(uucomp) === user.companyId && parseInt(uuagen) === user.agencyId && parseInt(uucomp) === user.subc && parseInt(uuagen) === user.suba) {
      const { igv } = await getCompanyParams(uucomp, uuagen)
      res.status(200).json({
        data: {
          username: user.username,
          session: true,
          igv,
          uucomp,
          uuagen
        },
        success: true,
        message: 'you are authorized'
      })
    } else {
      res.status(500).send({
        success: false,
        message: 'error en parametros'
      })
    }
  } catch (e) {
    res.status(500).send({
      success: false,
      message: e.message
    })
  }
})

router.post('/register', async (req, res) => {
  const { username, password, agencyId, companyId, email, firstName, lastName, address, numberPhone, docType, numDoc, personId } = req.body

  try {
    const { token, expires, message, success } = await registerUser(username, password, agencyId, companyId, email, firstName, lastName, address, numberPhone, docType, numDoc, personId)
    res.send({ token, expires, message, success })

  } catch (err) {
    res.status(500).send({ message: err.message, success: false })
  }
})

router.get('/verifyEmail', async (req, res) => {
  const { email } = req.body

  try {
    const verify = await verifyEmail(email)
    res.send({ data: verify, success: true })
  } catch (err) {
    res.send({ message: 'error en el proceso', success: false })
  }
})

module.exports = router
