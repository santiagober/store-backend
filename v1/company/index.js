const express = require('express')

const {
  createCompany
} = require('./../../controllers/companyController')

const router = express.Router()

//TODO: agregar validacion de que el token enviado pertenezca al mismo usuario de la peticion y no a otro(ahora si permite)
router.post('/create', async (req, res) => {
  const { company, agency } = req.body
  try {
    const { success, company, agency } = await createCompany(company, agency)
    res.send({ success, company, agency })
  } catch (err) {
    res.status(500).send({ message: err.message, success: false })
  }
})

module.exports = router
